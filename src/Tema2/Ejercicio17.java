package Tema2;

import com.sun.org.apache.xml.internal.resolver.readers.SAXCatalogParser;

import java.util.Scanner;

public class Ejercicio17 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 17");
        Scanner reader = new Scanner(System.in);
        Integer n1 = 0;
        Integer n2 = 0;
        Integer acumulador = 0;

        System.out.println("Ingrese su primer numero \n(el segundo numero deberá ser mayor)");
        n1 = reader.nextInt();

        do {
            System.out.println("Ingrese su segundo numero \n(el segundo numero debe ser mayor que el primero)");
            n2 = reader.nextInt();
        }while (n2 < n1);

        for (int i = n1; i <= n2; i++){
            System.out.println("Los numeros entre " + n1 + " y " + n2 + " son: " + i);
            if (i%2==0) {
                acumulador += i;
            }
        }
        System.out.println("Y la suma de los multiplos de dos son: " + acumulador);
    }
}
