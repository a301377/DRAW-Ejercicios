package Tema2;

import java.util.Scanner;

public class Ejercicio20 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 20");
        Scanner reader = new Scanner(System.in);
        Integer n = 0;
        n = reader.nextInt();

        if(n == 0){
            System.out.println(String.format("El factorial del número es: %s", 1));
        } else {
            System.out.println(String.format("El factorial del número %s es: %s", n, factorial(n)));
        }

    }

    private static long factorial(long number) {
        if (number <= 1) {
            return 1;
        }else
            return number * factorial(number-1);
    }
}
