package Tema2;

import java.util.Scanner;

public class Ejercicio19 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 19");
        Scanner reader = new Scanner(System.in);
        Integer hora = 0;
        Integer minuto = 0;
        Integer segundo = 0;

        System.out.println("Ingresa hora, minuto y segundo");
        hora = reader.nextInt();
        do {
            System.out.println("La hora no puede ser mayor o menor de 24 horas");
            hora = reader.nextInt();
        } while(!(hora >= 0 && hora < 24));

        minuto = reader.nextInt();
        do {
            System.out.println("Los minutos no pueden ser mayor o menor de 60 minutos");
            minuto = reader.nextInt();
        } while(!(minuto >= 0 && minuto < 60));

        segundo = reader.nextInt();
        do {
            System.out.println("Los segundos no pueden ser mayor o menor de 60 segundos");
            segundo = reader.nextInt();
        } while(!(segundo >= 0 && segundo < 60));

        System.out.println(String.format("Son las: %d:%d:%d", hora, minuto, segundo));
    }
}
