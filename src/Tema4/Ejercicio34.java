package Tema4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ejercicio34 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 34");

        Scanner reader = new Scanner(System.in);
        Double califas = 0.0;

        List<Double> califasList = new ArrayList<>();


        for (int i=0; i <= 9; i++){
            System.out.println("Ingrese su calificacion " + (i+1));
            califasList.add(reader.nextDouble());
            califas += califasList.get(i);
        }

        System.out.println("Media = " + califas/10);

    }
}
