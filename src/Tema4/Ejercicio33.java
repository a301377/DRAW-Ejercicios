package Tema4;

import java.util.Arrays;
import java.util.List;

public class Ejercicio33 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 33");

        List<String> nombres = Arrays.asList("Ruben", "Luis", "Bruce Wayne", "Clark Kent", "Beverly Marsh", "Harry Potter", "Peter Parker", "Nick Fury",
                "Melissa", "Flash Tomson", "M.J. Watson", "Barry Allen", "James Logan", "Matt Murdock", "Gwen Stacy", "Homero Simpson", "Sasha Grey", "Ariel Rebel",
                "Dillion Harper", "Mia Khalifa");

        for(String nombre : nombres){
            System.out.println(String.format("Hola mi nombre es %s", nombre));
        }

    }
}
