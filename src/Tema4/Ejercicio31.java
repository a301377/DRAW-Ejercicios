package Tema4;

import java.util.Scanner;

public class Ejercicio31 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 31");

        Scanner reader = new Scanner(System.in);
        System.out.println("Ingrese el primer numero");
        Integer n1 = Integer.valueOf(reader.nextInt());
        System.out.println("\t");
        System.out.println("Ingrese el segundo numero");
        Integer n2 = Integer.valueOf(reader.nextInt());

        System.out.println("Seleccione la opcion que desea realizar:");
        System.out.println("1. Suma");
        System.out.println("2. Resta");
        System.out.println("3. Multiplicacion");
        System.out.println("4. Division");
        System.out.print("Opcion: ");
        Integer opcion = Integer.valueOf(reader.nextInt());

        switch(opcion){
            case 1:
                System.out.println(String.format("La suma de %s + %s = %s ", n1, n2, n1 + n2));
                break;
            case 2:
                System.out.println(String.format("La resta de %s - %s = %s", n1, n2, n1 - n2));
                break;
            case 3:
                System.out.println(String.format("La multiplicacion de %s * %s = %s", n1, n2, n1 * n2));
                break;
            case 4:
                if(n2 == 0){
                    System.out.println("No se puede dividir entre cero");
                }else{
                    System.out.println(String.format("La division de %s / %s = %s", n1, n2, n1 / n2));
                }
                break;
        }

    }
}
