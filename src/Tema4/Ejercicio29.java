package Tema4;

import java.util.Random;

public class Ejercicio29 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 29");

        Random random = new Random();
        Integer contar = 0;


        for (int i = 0; i < 100; i++) {

            Integer d1 = 0;
            Integer d2 = 0;

            while(d1==0){
                d1 = random.nextInt(7);
            }
            while(d2==0){
                d2 = random.nextInt(7);
            }

            if(d1 + d2 == 10){
                contar++;
            }

        }

        System.out.println(String.format("En total de veces que sumaron 10 los dados fue: %s", contar));
    }
}

