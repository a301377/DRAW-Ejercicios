package Tema3;

import java.util.Scanner;

public class Ejercicio27 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 27");

        Scanner reader = new Scanner(System.in);
        Integer numero;

        System.out.print("Introduce un número entero: ");

        numero = reader.nextInt();
        if (numero >= 0 && numero <= 10){
            System.out.println("Tabla del " + numero);
            for(int i = 1; i<=10; i++) {
                System.out.println(numero + " * " + i + " = " + numero * i);
            }
        }else{
            System.out.println("Ingrese un numero entre el 0 y 10");
        }

    }
}
