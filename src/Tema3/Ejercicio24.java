package Tema3;

import java.util.Scanner;

public class Ejercicio24 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 24");

        Scanner reader = new Scanner(System.in);
        Integer aux = 0;
        Integer i = 0;
        Integer numero = 0;
        System.out.println("Ingrese numero");
        numero=reader.nextInt();

        for(i=1; i<(numero+1); i++){
            if(numero%i == 0){
                aux++;
            }
        }
        if(aux!=2){
            System.out.println("El numero no es primo");
        }else{
            System.out.println("El numero es primo");
        }

    }
}
