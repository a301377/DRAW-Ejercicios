package Tema3;

public class Ejercicio23 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 23");

        for (int i = 0; i <= 100; i++) {

            System.out.print(i);
            System.out.print("\t");

            if (i % 10 == 0) {
                System.out.print("\n");
            }

        }
    }
}
