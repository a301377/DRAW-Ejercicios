package Tema1;

import java.util.Scanner;

public class Ejercicio9 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 9");
        Integer numero = 0;

        Scanner reader = new Scanner(System.in);

        System.out.println("Introduzca un numero");

        numero = reader.nextInt();

        if (numero >= 0) {
            System.out.println("positivo");
        }else{
            System.out.println("negativo");
        }
    }
}
