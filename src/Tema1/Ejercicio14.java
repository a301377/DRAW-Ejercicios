package Tema1;

import java.util.Scanner;

public class Ejercicio14 {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Integer numero = 0;
        Integer mayor = 0;
        Integer menor = 0;

        for (int i = 1; i <= 5; i++){
            System.out.println("Ingrese un numero");
            numero = reader.nextInt();
            if (menor != 0 && mayor != 0) {
                if (numero > mayor) {
                    mayor = numero;
                }else if (numero < menor) {
                    menor = numero;
                }
            }else{
                menor = numero;
                mayor = numero;
            }
        }

        System.out.println(String.format("El numero mayor es: %s", mayor));
        System.out.println(String.format("El numero menos es: %s", menor));

    }
}
