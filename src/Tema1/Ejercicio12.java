package Tema1;

public class Ejercicio12 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 12");
        Integer acumPares = 0;
        Integer acumInpares = 0;

        for (int i=1;i<=100;i++){
            if (i%2==0) {
                acumPares+=i;
            }else{
                acumInpares+=i;
            }
        }
        System.out.println("Pares = " + acumPares);
        System.out.println("Inpares = " + acumInpares);
    }

}
