package Tema1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ejercicio11 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 11");
        Integer numero = 0;
        Integer contador = 0;
        List<Integer> numerosList = new ArrayList<>();

        Scanner reader = new Scanner(System.in);

        System.out.println("Introduzca un número");

        numero = reader.nextInt();

        System.out.println("Los multiplos de 3 son: ");

        for (int i = 1; i <=numero; i++){
            if (i%3==0) {
                System.out.println(i);
                contador ++;
            }
        }

        System.out.println("Y son un total de: "+ contador);
    }
}
