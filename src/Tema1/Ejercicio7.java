package Tema1;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ejercicio7 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 7");
        String frase = "";
        String res = "";
        List<String> frases = new ArrayList<>();
        Scanner reader = new Scanner(System.in);

        System.out.println("Introduce una frase tonto nauseabundo");
        frase = reader.nextLine();
        frases.add(frase);
        do {
            System.out.println("Deseas introducir más frases (S/N): ");
            res = reader.nextLine();
            if (res.equals("s") || res.equals("S")) {
                System.out.println("Introduce una frase tonto nauseabundo");
                frase = reader.nextLine();
                frases.add(frase);
            }
        }while (res.equals("s") || res.equals("S"));

        System.out.println("usted introdujo "+ frases.size() + " frases");

    }
}
