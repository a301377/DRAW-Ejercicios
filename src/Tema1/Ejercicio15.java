package Tema1;

import java.util.Scanner;

public class Ejercicio15 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 15");
        Integer numeroUno = 0;
        Integer numeroDos = 0;
        Integer contador = 0;
        Integer contadorPares = 0;
        Integer acumulador = 0;
        Scanner reader = new Scanner (System.in);

        System.out.println("Inserte su primer número");
        numeroUno = reader.nextInt();
        System.out.println("Inserte su segundo número");
        numeroDos = reader.nextInt();

        if (numeroUno > numeroDos) {
            for (int i = numeroDos; i<=numeroUno;i++){
                contador ++;
                if (i%2 == 0) {
                    contadorPares++;
                }else {
                    acumulador += i;
                }
            }
        }else{
            for (int i = numeroUno; i<=numeroDos;i++){
                contador ++;
                if (i%2 == 0) {
                    contadorPares++;
                }else {
                    acumulador += i;
                }
            }
        }

        System.out.println("Los numeros naturales entre ambos numeros son: " + contador + " de los cuales " + contadorPares +" son pares y la suma de los impares es: " + acumulador);



    }
}
