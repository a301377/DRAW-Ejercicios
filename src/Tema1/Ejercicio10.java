package Tema1;

import java.util.Scanner;

public class Ejercicio10 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 10");
        Integer numero = 0;

        Scanner reader = new Scanner(System.in);

        System.out.println("Introduzca un numero");

        numero = reader.nextInt();

        if (numero % 2 == 0) {
            System.out.println("Numero par");
        }else{
            System.out.println("Numero inpar");
        }
    }

}
