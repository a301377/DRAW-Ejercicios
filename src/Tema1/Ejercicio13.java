package Tema1;

public class Ejercicio13 {
    public static void main(String[] args) {
        System.out.println("--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--<<O>>--");
        System.out.println("Ejercicio 12");

        System.out.println("Los multiplos de 2 son: ");
        for (int i = 1; i <= 100; i++){
            if (i%2==0) {
                System.out.println(i);
            }
        }

        System.out.println("Los multiplos de 3 son: ");
        for (int i = 1; i <= 100; i++){
            if (i%3==0) {
                System.out.println(i);
            }
        }
    }
}
